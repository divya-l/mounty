import Vue from 'vue'
import App from './App.vue'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import Mounty from '@/mounty-components';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

// import 'bootstrap/scss/bootstrap.scss';


// Register components globally
for(let key in Mounty){
  Vue.component(key, Mounty[key])
}

Vue.config.productionTip = false
Vue.use(BootstrapVue);
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
