import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import CampDetails from './views/CampDetails.vue'
import RedirectPage from './views/RedirectPage.vue'

Vue.use(Router);
export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/camps/trolley-river-crossing-camping/in/manali'
        },
        {
            path: '/home',
            component: Home,
        },
        {
            path: '/camps/:camp_name/in/:place',
            component: CampDetails
        },
        {
            path:'/redirect/:page',
            component: RedirectPage
        }
    ]
})