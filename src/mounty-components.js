import McNavbar from './components/McNavbar.vue'
import McImageViewer from './components/McImageViewer.vue'
import McBreadCrumb from './components/McBreadCrumb.vue'
import McFooter from './components/McFooter.vue'
import McCampDetails from './components/McCampDetails.vue'
import McCarousel from './components/McCarousel.vue'
import McCampBook from './components/McCampBook.vue'

const Mounty = {
    "mc-navbar": McNavbar,
    "mc-image-viewer": McImageViewer,
    "mc-bread-crumb": McBreadCrumb,
    "mc-footer": McFooter,
    "mc-camp-details": McCampDetails,
    "mc-carousel": McCarousel,
    "mc-camp-book": McCampBook

}
export default Mounty;